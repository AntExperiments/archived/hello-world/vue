import axios, {AxiosInstance, AxiosRequestConfig, AxiosResponse} from "axios";

const apiEndpoint = 'https://geek-jokes.sameerkumar.website/api';

export default abstract class Api {
    private api: AxiosInstance;

    private static defaultConfig = {
        baseURL: apiEndpoint,
        headers: {
            'Content-Type': 'application/json',
        },
        timeout: 7500,
    }

    protected constructor(config: AxiosRequestConfig) {
        const mergedConfiguration = {...Api.defaultConfig, ...config};
        this.api = axios.create(mergedConfiguration);
    }

    public get<T, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
        return this.api.get(url, config);
    }

}